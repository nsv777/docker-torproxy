# set alpine as the base image of the Dockerfile
FROM alpine:latest

# update the package repository and install Tor
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories
RUN apk add --no-cache curl lyrebird tor

# Copy over the torrc created above and set the owner to `tor`
COPY torrc /etc/tor/torrc
RUN chown -R tor /etc/tor

# Set `tor` as the default user during the container runtime
USER tor

# Set `tor` as the entrypoint for the image
ENTRYPOINT ["tor"]

HEALTHCHECK --interval=60s --timeout=30s --start-period=60s \
    CMD curl -Ss --socks5 127.0.0.1:9050 https://check.torproject.org/api/ip

# Set the default container command
# This can be overridden later when running a container
CMD ["-f", "/etc/tor/torrc"]
